use log::*;
use serde_derive::*;
use serde_json;

const USER_ENDPOINT: &str = "api/v4/users/";
const PROJ_ENDPOINT: &str = "projects";

pub struct Instance {
    server: String,
}

impl Instance {
    pub fn new(server_url: &str) -> Instance {
        let mut server = server_url.to_string();
        if !server.ends_with('/') {
            server.push('/');
        }
        Instance { server }
    }

    pub fn public_user_projects(&self, user: &str) -> Vec<Project> {
        let request = format!("{}{}{}/{}", self.server, USER_ENDPOINT, user, PROJ_ENDPOINT);
        let body = match reqwest::get(&request) {
            Ok(mut answer) => match answer.text() {
                Ok(txt) => txt,
                Err(txt_err) => {
                    error!("Failed to parse answer as text or no text was sent! Endpoint @ \"{}\": {:?}", &request, txt_err);
                    return Vec::<Project>::new();
                }
            },
            Err(error) => {
                error!("Can not reach endpoint @ \"{}\": {:?}", &request, error);
                return Vec::<Project>::new();
            }
        };

        match serde_json::from_str(&body) {
            Ok(data) => data,
            Err(error) => {
                error!(
                    "JSON decoding failed: {:?}\n\nRequest: {:#}\nAnswer: {:#}",
                    error, &request, &body
                );
                Vec::<Project>::new()
            }
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Project {
    pub id: i32,
    pub description: String,
    pub name: String,
    pub name_with_namespace: String,
    pub path: String,
    pub path_with_namespace: String,
    pub created_at: String,
    pub default_branch: String,
    pub tag_list: Vec<String>,
    pub ssh_url_to_repo: String,
    pub http_url_to_repo: String,
    pub web_url: String,
    pub readme_url: Option<String>,
    pub avatar_url: Option<String>,
    pub star_count: u64,
    pub forks_count: u64,
    pub last_activity_at: String,
    pub namespace: Namespace,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Namespace {
    pub id: u64,
    pub name: String,
    pub path: String,
    pub kind: String,
    pub full_path: String,
    pub parent_id: Option<String>,
}
