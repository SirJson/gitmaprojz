use clap::*;
use clap::{App, Arg, ArgMatches};
use fern;
use shell::*;
use log::*;

mod gitlab;

#[derive(Debug, Clone)]
struct RuntimeInfo {
    pub user: String,
    pub server: String,
    pub target: String,
    pub ssh: bool,
    pub quiet: bool,
}

fn setup_logging(info: &RuntimeInfo) {
    let global_filter: log::LevelFilter;
    let tokio_filter: log::LevelFilter;
    let hyper_filter: log::LevelFilter;
    let reqwest_filter: log::LevelFilter;

    if info.quiet {
        global_filter = log::LevelFilter::Off;
        tokio_filter = log::LevelFilter::Off;
        hyper_filter = log::LevelFilter::Off;
        reqwest_filter = log::LevelFilter::Off;
    } else {
        global_filter = log::LevelFilter::Info;
        tokio_filter = log::LevelFilter::Error;
        hyper_filter = log::LevelFilter::Error;
        reqwest_filter = log::LevelFilter::Warn;
    }

    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}] <{}> {}",
                record.level(),
                record.target(),
                message
            ))
        }).level(global_filter)
        .level_for("tokio_reactor", tokio_filter)
        .level_for("hyper", hyper_filter)
        .level_for("reqwest", reqwest_filter)
        .chain(std::io::stdout())
        .apply()
        .unwrap();
}

fn parse_args() -> RuntimeInfo {
    let matches: ArgMatches = App::new("GitMaProjz")
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("USE_SSH")
                .short("s")
                .long("ssh")
                .help("If set to true we will clone everything with SSH instead of HTTP(S)")
                .required(false),
        ).arg(
            Arg::with_name("QUIET")
                .short("q")
                .long("quiet")
                .help("Disables all output to stdout")
                .required(false),
        ).arg(
            Arg::with_name("USERNAME")
                .value_name("USERNAME")
                .help("The Gitlab User from which we will clone all repositories. Example: superdev1337")
                .takes_value(true)
                .required(true),
        ).arg(
            Arg::with_name("SERVER")
                .value_name("SERVER")
                .takes_value(true)
                .help("Selects the source Gitlab Server since you could also host your own.\nYour input value should look like this <protocol>://<sitedns>.<tld>\nExample: https://gitlab.com")
                .required(true),
        ).arg(
            Arg::with_name("TARGET_DIR")
                .value_name("TARGET_DIR")
                .takes_value(true)
                .help("Sets the directory into each repository will be cloned with its own folder. Example: /home/mjsr/Projects")
                .required(true),
        ).get_matches();

    RuntimeInfo {
        user: matches.value_of("USERNAME").unwrap().to_string(),
        server: matches.value_of("SERVER").unwrap().to_string(),
        target: matches.value_of("TARGET_DIR").unwrap().to_string(),
        ssh: matches.is_present("USE_SSH"),
        quiet: matches.is_present("QUIET"),
    }
}

fn main() {
    let info = parse_args();
    setup_logging(&info);

    debug!("\nConfig\n{:#?}\n", info);
    let instance = gitlab::Instance::new(&info.server);
    let projects = instance.public_user_projects(&info.user);
    for project in projects {
        info!("⚙  Cloning project: {}", project.name);
        let url: String = if info.ssh {
            project.ssh_url_to_repo
        }
        else {
            project.http_url_to_repo
        };
        let mut target = std::fs::canonicalize(&info.target).expect("Invalid target directory");
        target.push(&project.name);
        match cmd!("git clone {} {}",&url,target.as_os_str().to_str().unwrap()).run() {
            Ok(_) => (),
            Err(shell_err) => error!("Cloning repository for project \"{}\" failed: {:?}", &project.name, shell_err)
        }
    }
}
