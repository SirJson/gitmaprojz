#!/bin/sh

# Because I don't think utilities should depend on a specific version of
# libc I made this little reminder script on how to create a musl build

echo "Build started..."

cargo +beta build --release --target=x86_64-unknown-linux-musl

echo "Done!"