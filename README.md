# GitMaProjz

Clones all your git repositories from GitLab on your local pc. Useful in cases like a system restore.

There are limitations though because I wrote it for my own little situation overhere.
Those limitations are:

* Linux only. Go and cry to Google about it.
* Only public repositories. There is nothing that would stop you to add authentication and clone EVERYTHING
* Works only with Gitlab. Sorry guys never really used GitHub but I'm pretty sure you can make this also work with it

You can at least choose between SSH or HTTPS cloning which is nice.

## Improvement List

- [ ] OSX and Windows Support. I probably wouldn't bother with the Google macro and just write something simpler. Afterall you can shell execute on all three major platform.
- [ ] Add that one or two additional calls to be authenticated. Yay! Now that tool will also automatically clone the private repositories to your disk. If you need that I's really not hard to implement.
- [ ] Figure out if GitHub is also cool enough to give you a list of all your projects and if so add GitHub Support.
- [ ] Auto-submodule checkout. You forget it, I forget it we all do. So if we clone automatically anyways why not also checkout submodules if there are any

## Where are the Binaries?

I will release Binaries for Linux x64 after Rust 2018 is released which is needed to make musl builds. So I guess until then build it yourself. It's not hard just one `cargo +beta build --release` away!